import gulp from "gulp";
import cp from "child_process";
import gutil from "gulp-util";
import rename from "gulp-rename";
import sass from "gulp-sass";
import cssnano from "gulp-cssnano";
import BrowserSync from "browser-sync";
import webpack from "webpack";
import webpackConfig from "./webpack.config.babel";
import merge from "merge-stream";
import fs from 'fs-extra';
import path from 'path';
import del from 'del';

// import "hugo-search-index" task
require('hugo-search-index/gulp')(gulp, gutil)

// import the "img" task
require('./gulp/image_tasks.babel.js');

const browserSync = BrowserSync.create();
const hugoBin = "hugo";
const defaultArgs = ["-v"];

gulp.task("default", ["build"]);

/**
 * build: Does all necessary tasks to build the complete site and place it in the public/ directory
 */
gulp.task("build", ["hugo", "hugo-search-index"]);


/**
 * clean: removes all output files and built javascripts/css
 */
gulp.task("clean", ["clean-hugo", "clean-js", "clean-css"])

/**
 * hugo: runs the hugo binary to build the hugo site.
 *  
 * depends -
 *   css : builds sass & minifies css files, placing the result in static/css/ for hugo to pick it up
 *   js  : builds javascript and bundles it with webpack, placing the bundle in static/js/ for hugo to pick it up
 *   img: the img task processes images in-place in static/images/, 
 *        so it must finish before running hugo.
 */
gulp.task("hugo", ["img", "css", "js"], (cb) => buildHugo(cb));


function buildHugo(cb, options) {
  const args = options ? defaultArgs.concat(options) : defaultArgs;

  return cp.spawn(hugoBin, args, {stdio: "inherit"}).on("close", (code) => {
    if (code === 0) {
      cb();
    } else {
      cb("Hugo build failed");
    }
  });
}

/**
 * clean-hugo: deletes hugo build output
 */
gulp.task("clean-hugo", (done) => remove('./public', done))

/** deletes the given directory, returning a promise that completes when deletion finishes */
function remove(folders, done) {
  del(folders)
    .then(() => done())
    .catch(done)
  
}

/**
 * css: Builds sass & minifies CSS files.  Results go to public/css/ to be served
 *      by the web server.
 *      Invokes browserSync in dev so changes can be streamed to the test browser
 */
gulp.task("css", buildCss);

function buildCss(onError) {
  return gulp.src("./src/css/*.*css")
      .pipe(sass().on('error', onError || sass.logError))
      .pipe(gulp.dest('./static/generated-css'))
      .pipe(cssnano().on('error', onError || gutil.log))
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('./static/generated-css'))
}

gulp.task("clean-css", done => remove('./static/generated-css', done))

/**
 * js: Builds JavaScript using webpack.  Loads webpack config from './webpack.conf.js'.
 *     The output bundle goes in public/js/
 *     Invokes browserSync in dev so changes can be streamed to the test browser
 */
gulp.task("js", cb => buildJs(cb));

function buildJs(cb, extraConfig) {
  var myConfig = Object.assign({}, webpackConfig);
  if(extraConfig) {
    myConfig = Object.assign(myConfig, extraConfig);
  }

  webpack(myConfig, (err, stats) => {
    if(err){
      gutil.log("[webpack] " + err)
      cb(err)
      return
    }
    gutil.log("[webpack]", stats.toString({
      colors: true,
      progress: true
    }));

    if (stats.hasErrors()){
      cb("[webpack] Compilation errors!")
      return
    }
    
    const generatedFile = stats.compilation.assets['app.js'].existsAt
    fs.stat(generatedFile, (err, appjs) => {
      if (err) {
        cb(`Could not find generated app.js (expected at ${generatedFile})\n  `, err)
        return
      }
      if (appjs.size > 50 * 1000){
        cb(`generated entry bundle "app.js" is ${appjs.size / 1000}KB!  Reduce it under 50KB!`)
        return
      } else if (appjs.size > 25 * 1000) {
        gutil.log(gutil.colors.yellow(`[WARNING] generated entry bundle "app.js" is ${appjs.size / 1000}KB!  That's big!`))
      }

      cb();
    })
  });
}

gulp.task("clean-js", done => remove(['./static/generated-js', './src/**/*.js', './src/**/*js.map'], done))

/**
 * Runs the development server and watches for changes.
 * 
 * Depends:
 *   build - do the normal build process
 */
gulp.task("server", ["build"], () => {
  browserSync.init({
    server: {
      baseDir: "./public"
    }
  });
  gulp.watch("./src/js/**/*.*", () => {
    browserSync.notify("rebuilding js...")
    buildJs(err => {
      if(err) {
        gutil.log(gutil.colors.red('[js] Error building JS: ') + err)
        browserSync.notify("JS build failed :(")
        return
      }

      //gotta copy the generated js to public/
      fs.copy('static/generated-js/', 'public/generated-js/', err => {
        if(err) {
          gutil.log(gutil.colors.red('[js] Error copying generated js to public/: ') + err)
          browserSync.notify("JS build failed :(")
          return
        }
      
        browserSync.reload();
      })
    })
  });
  gulp.watch("./src/css/**/*.*css", () => {
      buildCss(err => {
        gutil.log(gutil.colors.red('[css] Error building css: ') + err)
        browserSync.notify("CSS build failed :(")
      })
      .pipe(gulp.dest('./public/generated-css/'))
      .pipe(browserSync.stream())
  });
  gulp.watch([        //all the hugo directories
    "archetypes/**/*.*",
    "content/**/*.*",
    "data/**/*.*",
    "layouts/**/*.*",
    "static/*.*",                   // files in the root of 'static/'
    "static/!(generated-*)/**/*.*", // files deeper in 'static/' but not in 'generated-' (taken care of by the above watches)
    "themes/**/*.*",
    "config.toml"
  ], () => {
    browserSync.notify("rebuilding hugo...")
    buildHugo(err => {
      if(err){
        gutil.log(gutil.colors.red('[hugo] Error building hugo: ') + err)
        browserSync.notify("Hugo build failed :(");
      } else {
        browserSync.reload();
      }
    })
  });
});
