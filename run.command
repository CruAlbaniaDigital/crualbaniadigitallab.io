#! /bin/bash

set -e

cd `dirname $0`

openBrowser() {
  if command -v xdg-open > /dev/null 2>&1
  then
    xdg-open $1
  elif command -v gnome-open > /dev/null 2>&1
  then
    gnome-open $1
  fi
}

command -v hugo >/dev/null 2>&1 || { echo >&2 "I require hugo but it's not installed.  Install it from https://gohugo.io/"; openBrowser 'https://gohugo.io/' ; exit 1; }

openBrowser 'http://localhost:1313'
hugo server
