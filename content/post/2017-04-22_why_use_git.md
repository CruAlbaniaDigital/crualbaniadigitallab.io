+++
tags = ["git"]
date = "2017-04-22T18:53:41+02:00"
title = "Why use Git?"
categories = ["blog"]

+++

# What's the deal with Git?

Have you ever had to collaborate with multiple people on a word document?  You know how ridiculous it can look when you turn on track changes:

![Sample word doc with track changes](/images/post/2017-04-22_why_use_git/word_track_changes.jpg)

It gets even worse when multiple people have sent you their revisions over email attachments, and you have to somehow merge the document together.  Well, source code is even worse.  A normal project has hundreds to thousands of files, they change all the time in different ways, and you have to have some way to know which version is actively running on your server and which version is still under development.

Back when I first started programming I used to make a zip file of all my code, name it with the current date, and save it on another hard drive.  For many of you this is what you currently do.  Git is a better way.

Git was created by Linus Torvalds in 2005 in order to help the open-source team working on Linux.  It is a Version Control System (VCS) whose purpose is to keep track of all the changes that you've made to a project.  Since 2005, Git has become very mature with a lot of technologies and services being based around it.  It is the tool of choice for the Open-Source community because of how easy it is to share code with other people.

# Ok, so why should I use it?

For any of the following reasons:

* If you have a software project where you want to maintain a history of how all the files have changed over time, and who has made those changes
* If you have multiple people working on the same software project
* If you plan to make your software project open-source
* If your project is based off an open source project
* If you want to use all the great services offered by [Github](https://github.com) or [Gitlab](https://gitlab.com)

**You should NOT use Git if your situation is the following:**

* A wordpress website
* Another CMS where most of the content is in MySQL
* Managing word documents or another proprietary file format
* The majority of the data you want to store is binary files or images

Git does best when most of the files it has to work with are text files.  Fortunately this is how most code is written.  HTML, CSS, PHP, and JavaScript files are really just text files with a different file extension.  If you can open it in Notepad, Git will do a great job with it.

# How do I start using Git in my project?

### First you need a git program.  

If you're on Windows, you should use [Git for Windows](https://git-for-windows.github.io/) and [TortoiseGit](https://tortoisegit.org/).  On Mac I recommend [SourceTree](https://www.sourcetreeapp.com/) (you can also use it on Windows if you want).  On Ubuntu Linux you can install it with apt.

Git is first and foremost a command-line program.  TortoiseGit and SourceTree are great programs, but to really learn the Git concepts you should learn how to use the command line.  In this article I'll show you Git from the command line because the concepts are the same even in the GUI programs.

### Next you should use a tutorial

This is a great interactive tutorial for Git: https://try.github.io/levels/1/challenges/1  
It even shows you how the files change with the commands you enter.  You should do this tutorial.

After you've finished that tutorial, you're ready to put your project into Git.

### Now, create a repository and add all your files.

1. Open up the command line (or your GUI program), navigate to the folder, and initialize the git repository using `git init`.  
2. Add all the files using `git add *`.
3. Make sure you haven't missed any by running `git status`.  If you missed some, add them directly using `git add`.
4. Commit the files using `git commit -m "[your commit message]"`.  The commit message is important.  It tells people why you made this change.
5. Now look at your history using `git log`.  To quit the history viewer, you type `:q` and hit enter.

Here's a gif showing how I did it for the project [broshura-4-ligjet](https://gitlab.com/crualbaniadigital/broshura-4-ligjet):
![Gif showing the git init process](/images/post/2017-04-22_why_use_git/git_init.gif)

Congratulations!  You've successfully put your project under version control.  But this isn't magic.  You need to remember to make a commit every time you change the code.

# Ok now I have Git, what do I do with it?

Git is just the beginning of creating your development process.  Everyone needs a development process, but a tool won't create that for you.  A development process includes many tools, and sometimes the purpose of a tool is to get in your way and slow you down when you are about to do something foolish.

At Digital Strategies Albania, we have different processes for different contexts.  Our Wordpress websites are managed through CPanel and the wp-admin tool, where different people have different access levels.  You probably already do this, and you don't need Git for this.  Where we use Git, and where we have stricter development processes, are for our open-source web applications.  In the next post I'll explain more about our process for Hapitjeter, how we use automation and testing to improve the error-catching process and tighten the developer feedback loop so developers know faster when something goes wrong.