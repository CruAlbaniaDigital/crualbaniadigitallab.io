+++
date = "2017-04-18T14:00:00+02:00"
title = "Hosting a PHP application"
categories = ["tutorials"]
tags = ["lamp", "php", "hapitjeter"]
author = "gordon"

+++

## How to host a PHP application like Hapitjeter

[HapiTjeter]({{< relref "hapitjeter.md" >}}) is a standard PHP application that runs on the internet.  In this tutorial I'll show you how to use a public hosting service to install your own version of HapiTjeter, or any other PHP application.  We'll go through the steps all the way from downloading the code, through configuring your MySQL database, and finally setting up your DNS records so that you can use a custom domain.

## Table of Contents
* [Finding a host for your LAMP stack](#finding-a-host)
  * [Option 1: a fully integrated host like Hostgator](#option-1-using-hostgator)
  * Option 2: Rent a Virtual Private Server (covered in another post)
* [Uploading the Code via FTP](#uploading-the-code)
* [Setting up the Domain](#setting-up-the-document-root-for-the-domain)
* [Configuring the PHP Application](#configuring-the-php-application)
* [Creating the MySQL Schema](#creating-the-sql-schema)


# Finding a host

Hapitjeter requires a standard LAMP stack.  LAMP stands for Linux-Apache-Mysql-PHP, which are the 4 technologies that combine together to power Hapitjeter.  This is the same stack that powers Wordpress.  You may already have a Wordpress site, if so you can skip to the next section.  If you don't, there are a ton of options a quick Google search away.

A more advanced option is to rent a Virtual Private Server and set everything up yourself.  You can rent one for as little as $5 a month from Digital Ocean, but it requires you to use the Linux Command Line to set up the server.

Here at Albania Digital Strategies we use Hostgator, which sets us up with CPanel.  In the rest of this post I'll show you instructions using Hostgator and CPanel to deploy Hapitjeter.

## Option 1: Using Hostgator

Once you've purchased your domain and started your server on Hostgator, you can access CPanel.  With CPanel you have access to your MySQL databases.  You'll need to create a new database to use with your PHP application.

![CPanel create MySQL database](/images/post/2017-04-18_hosting_php_server_tutorial/cpanel_mysql_databases.png)

You also need to create a username and password for the application to use, and then set permissions for that user.

![CPanel create MySQL user](/images/post/2017-04-18_hosting_php_server_tutorial/cpanel_mysql_users.png)
![CPanel update user permissions](/images/post/2017-04-18_hosting_php_server_tutorial/cpanel_mysql_user_permissions.png)

<div class="note">
Make sure you write down the database name, username, and password.  You'll need all 3 when you configure the application.
</div>

Next you need an FTP account, so that you can upload the application files to the server.  You can also use the CPanel File Manager, but FTP is faster and easier to use once you've set it up.

![CPanel create FTP account](/images/post/2017-04-18_hosting_php_server_tutorial/cpanel_ftp_accounts.png)

After creating your FTP account you should be able to connect to the server using [FileZilla](https://filezilla-project.org/).  Download and install FileZilla then open it up to connect to the web server.  In the "host" field type the domain name of your web server.  The "user" field is your username, including the "@hostname" section (mine is gordon@jetestudentore.com).  The "password" field is the password for your FTP user.

You might get a dialog like the following if you are using Hostgator - this is because you didn't buy a SSL certificate for your domain yet and Hostgator wants to make sure your connection is secure.  

![FTP unknown certificate](/images/post/2017-04-18_hosting_php_server_tutorial/ftp_unknown_certificate.png)

<div class="note warning">
<b>DO NOT CLICK OK WITHOUT CHECKING!</b><br/> Ensure that you are actually connecting to your hostname, and that the certificate is actually registered to Hostgator.  If you are on a public connection like at a coffee shop, someone could very easily trick you into connecting to the wrong server and steal your information.
</div>



# Uploading the code

Now that you have a host and you've set up FTP you can connect to the web server and upload the code.  If this is the only website you host on this server, you should put the code in the "public_html" directory.

If this is not the only website you host on this server, you should create a subdirectory inside "public_html" and upload it there.  For this tutorial I will create a directory called "hapitjeter2".  Now you can upload the code by dragging and dropping it into that folder.  Note: if you downloaded the code using Git, you will have a folder called `.git` on your local computer.  You do not need to upload this to the server, it is very large and it is unnecessary.  Select everything else in the folder, and drag it over to the section showing the remote server.

![FTP uploading](/images/post/2017-04-18_hosting_php_server_tutorial/ftp_uploading.png)

# Setting up the document root for the domain

In this step we'll tell the server where to look when it receives a request for the application.  When you buy a domain from Hostgator, it will ask you to tell it where the "document root" is for this domain.  If this is the main website for this server, that should be "/public_html".  This means that whenever someone types your domain into their web browser, they will get the web page that is in "/public_html/index.php".  In our case, the domain is jetestudentore.com, and the website at "/public_html" is a wordpress website.

If you have another website already running at the document root, with it's own domain, you can either put the new application on a subdomain (for example, "hapitjeter.jetestudentore.com"), or on a separate domain entirely (like "hapitjeter.net").  In the first case, you need to go to the "subdomains" section of CPanel.  In the second case, you need to go to the "addon domains" section, and buy a new domain.  

For this tutorial I'll be putting the new application on a subdomain of "hapitjeter.net" called "test.hapitjeter.net".

![Hapitjeter as a subdomain](/images/post/2017-04-18_hosting_php_server_tutorial/cpanel_subdomain_hapitjeter.png)

Once the application has finished uploading, I should be able to see it at http://test.hapitjeter.net/.  Unfortunately, since I haven't configured it yet, it causes an error.

# Configuring the PHP application

Every application is configured differently.  Many use Environment Variables to configure themselves, and Hapitjeter supports this.  However, the more common way is to use a configuration file.  Hapitjeter supports this too.  For whichever application you use you should ask the developer how to configure it.  I'll show you how Hapitjeter is configured so that you can get an idea what you should ask for.

<div class="note"><b>The following instructions are specific to Hapitjeter.</b>  Other PHP applications are configured differently.</div>

Hapitjeter has a setup page at `/setup.php`.  This setup page will help you create the configuration file and test your connection to the SQL database.  I'll access it at http://test.hapitjeter.net/setup.php 

![Hapitjeter config page](/images/post/2017-04-18_hosting_php_server_tutorial/hapitjeter_config_page.png)

Once I fill in the information, and Hapitjeter checks that it can access the database, I receive the configuration file.  The MySQL server hostname at Hostgator is "localhost", and the "username" and "password" are those that I created for the MySQL user above.  "MySQL Schema Name" is the name of the database I created.
You can also select the language, but you should provide translations inside the folder "includes/lang".

![Hapitjeter config download](/images/post/2017-04-18_hosting_php_server_tutorial/hapitjeter_config_page.png)

This is what the file looks like:

```ini
[host]
 ; The canonical root address of your application, including relative path
url.canonical = 'http://test.hapitjeter.net/'
url.relative_path = '/'     ; The relative path of the root of your application

[mysql]
mysql.host   = 'localhost' ; the hostname where the mysql server is located, usually localhost
mysql.user   = 'adzefi_hapitjet2' ; the username in the mysql server that the application uses to connect
mysql.pass   = '****************' ; the password of the user
mysql.schema = 'adzefi_hapitjeter_2' ; the name of the schema that hapitjeter will use

[site]
site.lang  = 'al'  ; the language of the site - translations in includes/lang/
```

Now I have to do is copy that configuration file to the correct place using FTP, and the app should read the configuration and connect to the database.

![FTP upload the config file](/images/post/2017-04-18_hosting_php_server_tutorial/ftp_upload_config_ini.png)

![Hapitjeter config filled in](/images/post/2017-04-18_hosting_php_server_tutorial/hapitjeter_config_filled.png)

# Creating the SQL Schema

There's one more step before Hapitjeter will work - we need to create a schema in the SQL database.  I happen to have a snapshot of the current Hapitjeter which I will upload to the database using CPanel, which will not only create the schema but will also add the lessons, users, and messages.

<p class="note">
If you need a snapshot for your own clone of Hapitjeter, you can <a href="{{< relref "contact_us.md" >}}">contact us</a> and we'll help you out.
</p>

To do operations on the MySQL database, we'll use the PhpMyAdmin section of CPanel.  I go to "Database" -> "adzefi_hapitjeter_2" -> "Import", and select my snapshot file for upload.  Then I hit "Go".

![Loading the SQL dump with PhpMyAdmin](/images/post/2017-04-18_hosting_php_server_tutorial/phpMyAdmin_import_sql_dump.png)

And now, in the end, we have arrived at a working clone of Hapitjeter!

![The working Hapitjeter clone](/images/post/2017-04-18_hosting_php_server_tutorial/test_hapitjeter_net.png)

