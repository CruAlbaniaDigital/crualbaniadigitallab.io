+++
date = "2017-04-08T15:39:47+02:00"
title = "What is this blog?"
categories = ["blog"]
tags = []
author = "gordon"

+++

## What is this blog?

This blog is where Cru Digital Strategies Albania will talk about the various projects that we're working on, who is working on them, and how you can get involved.  The purpose is to get more people excited about sharing code and getting involved.

## Who's it for?

Anyone who has a passion for digital content and wants to reach people for Christ through digital media!  We welcome any and all help and have a number of ways you can get involved.

Are you a:

* Programmer?  [Sign in to Gitlab and join our team!](https://gitlab.com/crualbaniadigital)
* IT Professional?  We'd love to have your help in managing our websites!  [Contact Us!]({{< relref "contact_us.md" >}})
* Student?  Want to learn about programming?  Join one of our projects and learn from our professionals!
    * [broshura-4-ligjet](https://gitlab.com/crualbaniadigital/broshura-4-ligjet) HTML and JavaScript
    * [HapiTjeter]({{< relref "hapitjeter.md" >}}) PHP 5
* Idea person?  [Come to the next Indigitous #HACK!](https://indigitous.org/)
* Evangelist?  Have a look around and learn how to use our digital tools!