+++
tags = []
date = "2017-06-03T10:25:00+02:00"
title = "Checking Broken Links"
categories = []

+++

We've all seen ugly 404 urls when a website links to something that no longer exists.  It's annoying to users when they can't find something that was explicitly linked to.  The problem is it's really hard to avoid broken links.  Every website maintainer has at one point or another forgotten a link to a page that he has renamed.  What is there to do?

## Link Checker

A really great tool to use regularly on your sites is [Link Checker by Bastian Kleineldam](https://wummel.github.io/linkchecker/).  It runs on windows, mac, and linux with a GUI or command line interface.  This makes it easy to use as a tool or also in an automated process.  On windows, it's very easy to install and run the GUI.  Here's an example of a scan I did of http://sotmeria.org

![Sotmeria.org scan](/images/post/2017-06-03_checking_broken_links/link_checker_results.png)

You can see I have a broken link!  I didn't realize I had a broken link, but I remembered I recently renamed the page `/young-professionals` to the Albanian `/profesionistet`.  I had just forgotten to update one of the links :)

I went to the webpage and searched the HTML for `young-professionals` using the Mozilla firefox dev tools, and found that the broken link was in my headline image slider.  One of the slides linked to the old name.  So, I just went into the wp-admin panel and fixed the link.

![fixing the broken link](/images/post/2017-06-03_checking_broken_links/link_check_fixing.gif)

## Automating the process

I'm a strong believer in automated processes.  Automation saves us from the time intensive and (most of the time) useless tasks that we often perform every day in managing our websites.  An automated process requires three things - a tool, an automatic task runner, and a way to notify you of failures.  Today I'm going to show you a way to automate this process by running Link Checker automatically using a cron job.  We'll do the following steps:

1. [download the free-standing version of linkchecker](#download-as-a-tar-gz)
2. [upload the unzipped version to cpanel](#upload-it-to-cpanel)
3. [create a cron job on cpanel to run linkchecker](#set-up-a-cron-job)
4. [Test that the cron job results get emailed](#check-your-email)

<div class="note warning">
Linkchecker requires python version 2.7.2 or higher, so you need to make sure that's installed on your system.  You can do that by logging in using SSH and typing "<code>python --version</code>"
</div>

### Download as a tar.gz

In addition to downloading as an installer, you can also get a free-standing version of linkchecker in a `tar.gz` file.  `tar.gz` is just like `zip`, you just need to extract it.  7Zip can do that.  Go ahead and download `linkchecker.tar.gz` and extract it in your downloads directory.

### Upload it to cpanel

We need to upload the linkchecker binary file to the server using CPanel.  Go to the file manager, click "upload" and move the zip file to the server.  Once you've done that, go back to the file manager and extract it.

![Upload linkchecker](/images/post/2017-06-03_checking_broken_links/upload_linkchecker.png)
![Extract linkchecker](/images/post/2017-06-03_checking_broken_links/extract.png)

### Set up a cron job

Now we need a cron job that will do the link checking on a schedule, and email us if anything goes wrong.  CPanel has a tab called "Cron jobs" - you should navigate there.

The first setting is called "Cron email" - this is the email address where the results of cron jobs get sent.  Put your email address there and click "update email".

The next setting is "Add new cron job".  We'll use one of the common settings in the drop down, "Once Per Hour" or `0 * * * *`.  This cron job will be run every hour on the hour.  Feel free to play with the scheduling if you like.

Finally, we need to ensure that the cron job will only send an email if the link check failed.  We don't want to be getting emails every hour even if there were no broken links.  So, we use a bit of Bash magic by having LinkChecker write its results to a file, and then we use `cat` to show the results of the file only if LinkChecker failed.

```bash
/home1/adzefi/LinkChecker-9.3/linkchecker --ignore-url xmlrpc.php --ignore-url wp-json -Ftext//tmp/linkcheck_out.txt http://sotmeria.org 2>/dev/null || cat /tmp/linkcheck_out.txt
```

Let's break this down line by line.  First we have this: `/home1/adzefi/LinkChecker-9.3/linkchecker`.  This is the full path to the linkchecker program which we uploaded earlier.  Yours will be different - you need to put in the right home directory.  You can find it in the File Manager.

Second we have this: `--ignore-url xmlrpc.php --ignore-url wp-json`.  These are a couple broken links that I don't care about and I want LinkChecker to ignore.  For these links it won't generate any lines in the report and won't fail the check.

Third is this: `-Ftext//tmp/linkcheck_out.txt`.  This is telling Link Checker to write its output to a file as a report.  We'll use this later.

Next: `http://sotmeria.org`  the URL of the website I want to test.

Now the bash magic:  
`2>/dev/null` tells LinkChecker to stop writing junk to the console, because we only want to write junk to the console if there's a real error.
`|| cat /tmp/linkcheck_out.txt` says that if linkchecker fails, run the "cat" command to send me the output of the report file.

After you've replaced the lines in the example with the appropriate ones for your website, you can put it in the cron job in CPanel.

![add cron job](/images/post/2017-06-03_checking_broken_links/cron_job_added.png)

### Check your email

Great!  Your cron job is running.  Once the clock rolls around to the next hour, you should check your spam email folders for an email from Cron Daemon.  If a link is broken, you'll get an email with the results.  If you get an email that says `cat: /tmp/linkcheck_out.txt: No such file or directory`, that means that something's wrong and you should check to see if you've got the correct python version available.

I hope this inspires you to try to automate more things!  It certainly has for me.  I've even built broken link checking into Crubot, the friendly chatbot who listens in on [our gitter chatroom](https://gitter.im/CampusCrusade/lobby).  Stop by and say hi sometime!

![Link checks from gitter](/images/post/2017-06-03_checking_broken_links/gitter_link_check.png)


