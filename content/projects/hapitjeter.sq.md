+++
name = "Hapitjetër"
gitlab = "crualbaniadigital/hapitjeter"
gitlab_id = 2711660
webpage = "http://hapitjeter.net"
docs = "https://gitlab.com/crualbaniadigital/hapitjeter/wikis/home"
feature_image = "/images/projects/hapitjeter/homepage.png"
short_description = "Faqje për të mesuar një dishepull përmes video dhe mesime online"
+++

![kryefaqja per Hapitjeter](/images/projects/hapitjeter/homepage.png)

Hapitjetër është një faqje me të cilen mund të japesh mesime një dishepulli me video dhe mesime online.  Kur njeri regjistrohet tek webfaqja, ai zgjedh një mentor nga listi.  Ky mentor i kontrollon përgjigjet e dishepullit, duke korrigjuar dhe mesuar atë gjatë proçesit.  Kur dishepulli vjen në fund të kursit, mund të japë edhe ai mentorim për një dishepull tjetër.  Emri i tij vendoset në list keshtu që mund të zgjedhet kur tjetri regjistrohet.

![Të plotësosh një përgjigje](/images/projects/hapitjeter/lesson_write_answer.png)

Kur mund të jetë i ngatërruar, në çdo pikë mund t'i nisë një mesazh mentorit.

![Mesazhet](/images/projects/hapitjeter/messages.png)

Ne do të kishim shumë qejf të ju ndihmojmë ta përdorni këtë projekt në sherbesën tuaj!  [Shkarkoni programin](https://gitlab.com/crualbaniadigital/hapitjeter) dhe [lexoni udhëzimet ta konfiguroni](https://gitlab.com/crualbaniadigital/hapitjeter/wikis/home).

[Na nisni një mesazh]({{< relref "contact_us.md" >}}) për çfarëdo llojë pyetje tjetër!