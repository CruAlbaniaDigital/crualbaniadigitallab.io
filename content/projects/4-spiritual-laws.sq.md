+++
name = "Webfaqja 4 Ligjet Shpirterorë"
gitlab = "crualbaniadigital/broshura-4-ligjet"
webpage = "http://crualbaniadigital.gitlab.io/broshura-4-ligjet/"
feature_image = "/images/projects/4-spiritual-laws/homepage.png"
+++

![Faqja në veprim](/images/projects/4-spiritual-laws/homepage.png)

Ky projekt tregon të katër ligjet shpirterorë në një faqje internet.  Këtë projekt e krijoi një student, dhe e kemi vendosur online tek http://crualbaniadigital.gitlab.io/broshura-4-ligjet/.  Mund t'ia dergoni ndonjë personi dhe kur ta mbarojë leximin, do të plotësojë një form kontakt që na drejtohet neve.

Ky është një projekt i thjesht, që ndertohet në HTML dhe JavaScript.  Vendoset lehtësisht me një web-server si [Apache](https://httpd.apache.org/).

