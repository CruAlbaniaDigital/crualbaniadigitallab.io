+++
name = "4 Spiritual Laws webpage"
gitlab = "crualbaniadigital/broshura-4-ligjet"
webpage = "http://crualbaniadigital.gitlab.io/broshura-4-ligjet/"
feature_image = "/images/projects/4-spiritual-laws/homepage.png"
+++

![Page in action](/images/projects/4-spiritual-laws/homepage.png)

This was a project completed by a student, which shows the 4 spiritual laws online.  We have it hosted at http://crualbaniadigital.gitlab.io/broshura-4-ligjet/.
You can send the link to someone and when they finish reading it, at the end they will fill in a contact form which goes to our email.

It's a simple HTML and JavaScript app, which can be easily set up behind a web server like [Apache](https://httpd.apache.org/).