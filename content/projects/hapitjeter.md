+++
name = "Hapitjeter"
gitlab = "crualbaniadigital/hapitjeter"
gitlab_id = 2711660
webpage = "http://hapitjeter.net"
docs = "https://gitlab.com/crualbaniadigital/hapitjeter/wikis/home"
short_description = "An online discipleship tool with interactive lessons and videos"
feature_image = "/images/projects/hapitjeter/homepage.png"
+++

![Hapitjeter homepage](/images/projects/hapitjeter/homepage.png)


Hapitjeter is an online discipleship tool that allows non-believers and young believers to engage in discipleship lessons online.
When they are invited to register on the site, they select a mentor from a drop-down list.  This mentor reviews the answers to
their questions and helps them understand the content of the course.  When they pass the course they can also mentor others.


![Filling in an answer](/images/projects/hapitjeter/lesson_write_answer.png)

If they are confused, at any point they can send a message to their mentor.

![Messages](/images/projects/hapitjeter/messages.png)

We would love to help you translate the site and include it in your ministry!  Feel free to [download the source code](https://gitlab.com/crualbaniadigital/hapitjeter)
and read the [documentation on how to set it up](https://gitlab.com/crualbaniadigital/hapitjeter/wikis/home).

[Contact Us]({{< relref "contact_us.md" >}}) for any other issues!