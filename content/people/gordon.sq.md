+++
name = "Gordon"
bio = ""
github = "gburgett"
gitlab = "gordon.burgett"
homepage = "https://www.gordonburgett.net"
email = "gordon@gordonburgett.net"

+++

# Gordon Burgett

Gordoni ka mbaruar për Inxhenir Elektrik por ne fakt ka punuar 5 vjet si Inxhenir Informatik.  Ka punuar me Microsoft .NET me full-stack development.  Aktualisht punon per Instutitin "Jeta e Re" në Tiranë duke bërë programim, dhe ka për zemër të zhvillojë lëvizjen midis Profesionistëve në Shqipëri.

Gordoni është një drejtues për mundimin e Institutit t'i japë të gjithë programet e tij me burim te hapur, kështu që të bëhet më lehtë për programet të shpërndahen në komunitetin më i gjerë.  Nëse do të donit të zbatonit një nga projektet tona për ta përdorur në shërbesën tuaj, flisni me Gordonin!

Në kohën e tij të lirë punon me [Sotmërinë](http://sotmeria.org/) që të ndihmojë profesionistët ta njohin në mënyrë të thellë Krishtin dhe të bëhen dishepuj shumëfishues.

![Gordoni me profesionistët](/images/people/gordon/yp_committe.jpg)

Gordoni ka qejf gjithashtu Ultimate Frisbee dhe barbekue.  Ai shikon shpesh ndeshjet e skuadres së tij Bejsboll, Texas Rangers.  Gjithmonë shkon për një ndeshje kur gjendet në Dallas gjatë sezionit.

[Shikoni websitin e tij personal!](https://www.gordonburgett.net)

#### Dhe tani shijoni një foto të bukur për perendimin i Diellit në Korfuz

![Perendimi në Korfu](/images/people/gordon/corfu_sunset.jpg)
