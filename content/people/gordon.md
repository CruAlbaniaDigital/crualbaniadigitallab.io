+++
name = "Gordon"
bio = ""
github = "gburgett"
gitlab = "gordon.burgett"
homepage = "https://www.gordonburgett.net"
email = "gordon@gordonburgett.net"

+++

# Gordon Burgett

Gordon is a software engineer with 5 years experience in full-stack development on the .NET platform.  Nowadays he works for CRU in Albania on digital strategies, and has a passion for seeing young adults come to know the LORD.

Gordon is one of the leaders of CRU Albania's effort to open-source all their code, and to make it easier to share that code with the wider missions comunity.  If you'd like to adapt one of our projects for use in your ministry, Gordon is the guy to talk to!

In his spare time he works with [Sotmëria](http://sotmeria.org/), helping other young professionals to know Christ more deeply and to become multiplying disciples.

![Gordon with the young professionals](/images/people/gordon/yp_committe.jpg)

Gordon also loves Ultimate Frisbee and Texas Barbeque.  Often you can find him watching his favorite Baseball team, the Texas Rangers.  He always attends a game when he's in Dallas during baseball season.

[Check out his personal blog to read more!](https://www.gordonburgett.net)


#### And now here's a beautiful photo of the sunset on the Greek island of Corfu

![Corfu sunset](/images/people/gordon/corfu_sunset.jpg)
