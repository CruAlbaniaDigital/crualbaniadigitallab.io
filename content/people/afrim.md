+++
name = "Afrim"
bio = ""
gitlab = "afrim26"
email = "lavdiafrim26@gmail.com"

+++

# Afrim Karoshi

Afrim is a digital strategist from Albania. He worked on different ministry responsibility for CRU in Albania, since he joined the movement in 1996 as a student, and later, in 2000 as a full time staff.

Afrim is committed to start Christ-centered movements in Albania and beyond, using digital strategies to connect with this generation. He leads a team of 5 people, who are involved in various projects aiming to start spiritual conversations among people in Albania.

In his spare time he writes comments about the Albanian culture in his personal blog [Kuturu](http://kuturu.org)

![A screenshot of Kuturu.org](/images/people/Afrim/kuturu-screenshot.jpg)

