import webpack from 'webpack'
import path from 'path'

export default {
  resolve: {
     // start with all typescript and javascript files
    extensions: ['.ts', '.tsx', '.js', '.jsx'],

    alias: {
      'react': 'preact-compat',
      'react-dom': 'preact-compat',
      // Not necessary unless you consume a module using `createClass`
      'create-react-class': 'preact-compat/lib/create-react-class'
    }
  },

  module: {
    loaders: [
      {
          // any referenced images should become files in the output directory
        test: /\.((png)|(eot)|(woff)|(woff2)|(ttf)|(svg)|(gif))(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file?name=/[hash].[ext]'
      },
      { test: /\.json$/, loader: 'json-loader' },
      {
        // Typescript compiled by awesome-typescript-loader
        // targeting ES2015 then passed to Babel for backwards compatibility.
        loaders: ['babel-loader', 'awesome-typescript-loader'],
        test: /\.tsx?$/,
        exclude: /node_modules/
      }
    ]
  },

  externals: {
        // require("jquery") is external and available
        //  on the global var jQuery.  Lodaed in layouts/partials/top.html
    jquery: 'jQuery'
  },

  devtool: 'source-map',

  plugins: [
      // Define the 'production' environment variable to silence React
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
      // Add a require() for the given dependencies when these names are used.
    new webpack.ProvidePlugin({
      'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch',
      URI: 'urijs',
      SearchIndex: 'search-index/dist/search-index.min.js'
    }),
      // separate out the common code dynamically loaded from our other chunks,
      // the ones that are loaded by app.ts after it has done the important stuff.
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      chunks: ['gitlab', 'search'], // The modules whose common code we want to split out
      minChunks: 2,                 // The min number of commonalities
                                    //   (i.e. if React is defined in 2 files split it out)
      async: true                   // Async-load the common file itself using Webpack, don't
                                    //   make us load it by including it in a <script> tag.
    }),
      // Minify the output files
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true
    })
  ],

  context: path.join(__dirname, 'src'),
  entry: {
      // babel-regenerator-runtime needed for Promise support
    app: ['babel-regenerator-runtime', './js/app']
  },
  output: {
      // put the output bundles in static/generated-js so it's included by Hugo
    path: path.join(__dirname, 'static/generated-js'),
    publicPath: '/generated-js/',
    filename: '[name].js',
    chunkFilename: '[name].js'
  }
}
