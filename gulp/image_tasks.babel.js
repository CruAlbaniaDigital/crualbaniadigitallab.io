import { Transform } from 'stream'

import gulp from 'gulp'
import gutil from 'gulp-util'
import imageResize from 'gulp-image-resize'
import imagemin from 'gulp-imagemin'
import fs from 'fs-extra'
import path from 'path'
import { execFile } from 'child_process'
import gifsicle from 'gifsicle'

/**
 * img: Processes images in-place in the static/images/ folder.
 *      Images are resized to a max width of 640x and compressed to save
 *      bandwidth.  Includes checks so it will do nothing if images are already
 *      processed.  Tasks loaded from './gulp_image_tasks.babel.js'
 *
 *      Images in both the 'images/' and 'images.original/' folder should be
 *      checked in to git in order to reduce the image processing work that has
 *      to be done on builds.
 *
 * depends -
 *   img-mirror: copies any new images from 'images/' to '.original-images' to save the originals
 *   img-make-640x: resizes and compresses images from '.original-images' over their equivalent in 'images/'
 *   gif-optimize: resizes and compresses gifs from '.original-images' over their equivalent in 'images/'
 */
gulp.task('img', ['img-mirror', 'img-make-640x', 'gif-optimize']);

/**
 * Copy unprocessed source images to the mirror folder, where the originals are kept unchanged.
 */
gulp.task('img-mirror', () =>
    gulp.src('./static/images/**/*.{jpg,png,gif,svg}')
        // filter out files where the mirror already exists - these have been processed.
      .pipe(filterIfCopyExists(srcToMirror))
        // copy the files to the mirror folder
      .pipe(gulp.dest(srcToMirror))
)

/**
 * Make the 640x version of the original source images for phones
 */
gulp.task('img-make-640x', ['img-mirror'], () => {
        // note- not processing GIFs due to a problem with gif resizing
  gulp.src('./static/.original_images/**/*.{jpg,png,svg}')
        // filter out files where the copy in the src directory has already been processed (it's not the same size)
      .pipe(filterIfCopyNotSameSize(mirrorToSrc))
        // resize the images from the mirror folder
      .pipe(imageResize({
        width: 640,
        noProfile: true
      }))
      .pipe(imagemin())               // compress the resized images
      .pipe(gulp.dest(mirrorToSrc))   // write the resized image back to the src folder
})

gulp.task('gif-optimize', ['img-mirror'], () => {
  return gulp.src('./static/.original_images/**/*.gif', { read: false })
        // filter out files where the copy in the src directory has already been processed (it's not the same size)
      .pipe(filterIfCopyNotSameSize(mirrorToSrc))

        // optimize the source gif into the correct place
      .pipe(new Transform({
        objectMode: true,

        transform (file, encoding, callback) {
          execFile(gifsicle,
            [
              '--output', path.join(mirrorToSrc(file), file.relative),
              '--resize-width', '640',
              '--colors', '256',
              '--optimize=3',
              file.path
            ],
            (err, stdout, stderr) => {
              if (err) {
                // console.log(stdout)
                console.error(stderr)
                callback(err)
                return
              }

              callback(null, file)
            }
          )
        }
      }))
})

// converts a Vinyl file's path in the src directory (/images/) to the mirror directory (/.original_images/)
const srcToMirror = file =>
            path.join(path.dirname(file.base), '.original_' + path.basename(file.base))

// the reverse of srcToMirror
const mirrorToSrc = file =>
            path.join(path.dirname(file.base), path.basename(file.base).replace('.original_', ''))

// a transform that simply logs every Vinyl file passed to it
/*
const logFiles = new Transform({
  objectMode: true,

  transform (file, encoding, callback) {
    gutil.log('base: ', file.base, 'path: ', file.path, 'cwd: ', file.cwd, 'stat', file.stat)
    callback(null, file)
  }
}) */

// A filter transform that removes from the stream files which are found in the mirror directory
const filterIfCopyExists = (findInMirror) => new Transform({
  objectMode: true,

  transform (file, encoding, callback) {
    var to = path.join(findInMirror(file), file.relative)

    fs.exists(to, exists => {
      if (exists) {
            // this file is already processed - skip it
        callback(null, null)
        return
      }
      callback(null, file)
    })
  }
})

// A filter transform that removes from the stream files which are not the same size as their mirrors
// (meaning they've already been processed)
const filterIfCopyNotSameSize = (findMirror) => new Transform({
  objectMode: true,

  transform (file, encoding, callback) {
    var to = path.join(findMirror(file), file.relative)
    fs.stat(to, (err, stats) => {
      if (err) {
        gutil.log('Error stat-ing: ', err, file.path)
        callback(err, null)
        return
      }
      if (file.stat.size !== stats.size) {
          // the two files are not the same size, filter this out
        callback(null, null)
        return
      }
        // the two files are the same size, pass this to the next transform in the processing chain
      callback(null, file)
    })
  }
})
