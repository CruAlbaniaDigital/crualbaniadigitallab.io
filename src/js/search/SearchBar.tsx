import {SearchStore} from 'hugo-search-index'
import * as React from 'react'

interface ISearchBarProps {
    store: SearchStore
    lang?: string
}

class SearchBar extends React.Component<ISearchBarProps, { text: string }> {

    private input: HTMLInputElement

    constructor(props) {
        super(props)
        this.state = { text: '' }
        this.handleChange = this.handleChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    public componentDidMount() {
        this.input.focus()
    }

    public render() {
        return (
          <form onSubmit={this.onSubmit}>
            <input type="search" placeholder="search..."
                value={this.state.text}
                onChange={this.handleChange}
                ref={(input) => this.input = input}
              ></input>
              <i className="fa fa-search" onClick={this.onSubmit}></i>
          </form>
        )
    }

    private onSubmit(event) {
        event.preventDefault()
        const text = this.state.text

        if (!text || text === '' || text === this.props.store.currentQuery) {
            // no need to run search
            return
        }

        this.props.store.runSearch(text, this.props.lang, (err, results) => {
            if (err) {
                console.error('error running search: ', err)
                return
            }
            // tslint:disable-next-line:no-console
            console.log('ran search for ', text)
        })
    }

    private handleChange(event) {
        this.setState({ text: event.target.value })
    }
}

export default SearchBar
