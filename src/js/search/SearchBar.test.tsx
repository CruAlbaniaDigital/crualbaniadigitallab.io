/* tslint:disable:no-unused-expression */

import * as chai from 'chai'
import * as chaiEnzyme from 'chai-enzyme'
import { shallow } from 'enzyme'
import {SearchStore} from 'hugo-search-index'
import * as React from 'react'
import * as sinon from 'sinon'

chai.use(chaiEnzyme()) // Note the invocation at the end
const expect = chai.expect

import SearchBar from './SearchBar'

describe('<SearchBar />', () => {
  it('should render text input field', () => {

    // act
    const rendered = shallow(<SearchBar store={undefined} />)

    expect(rendered).to.not.be.undefined
    const input = rendered.find('input')
    expect(input).to.have.prop('placeholder', 'search...')
  })

  it('should not run a search when submitting empty text', () => {
    const mRunSearch = sinon.mock().rejects('should not be called')
    const store = {
      runSearch: mRunSearch,
    }

    const rendered = shallow(<SearchBar store={store as any} />)

    // act
    rendered.find('form').simulate('submit', { preventDefault: () => false })

    expect(mRunSearch.called).to.be.false
  })

  it('should not run a search with the same text as current query', () => {
    const mRunSearch = sinon.mock().rejects('should not be called')
    const store = {
      currentQuery: 'test1234',
      runSearch: mRunSearch,
    }

    const rendered = shallow(<SearchBar store={store as any} />)
    rendered.setState({ text: 'test1234' })

    // act
    rendered.find('form').simulate('submit', { preventDefault: () => false })

    expect(mRunSearch.called).to.be.false
  })

  it('should run a search with new text', () => {
    const mRunSearch = sinon.mock().resolves(null)
    const store = {
      currentQuery: 'test1234',
      runSearch: mRunSearch,
    }

    const rendered = shallow(<SearchBar store={store as any} />)
    rendered.setState({ text: 'new query' })

    // act
    rendered.find('form').simulate('submit', { preventDefault: () => false })

    expect(mRunSearch.calledWith('new query')).to.be.true
  })
})
