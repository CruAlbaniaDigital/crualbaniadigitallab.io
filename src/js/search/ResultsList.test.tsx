/* tslint:disable:no-unused-expression */

import * as chai from 'chai'
import * as chaiEnzyme from 'chai-enzyme'
import { shallow } from 'enzyme'
import {SearchStore} from 'hugo-search-index'
import * as React from 'react'
import * as sinon from 'sinon'

chai.use(chaiEnzyme()) // Note the invocation at the end
const expect = chai.expect

import ResultsList from './ResultsList'

describe('<SearchResults />', () => {
  it('should render null with no results', () => {

    const store = new SearchStore(undefined)

    // act
    const rendered = shallow(<ResultsList store={store} />)
    expect(rendered).to.be.blank()
  })

  it('should render "no results" after a search returns empty', () => {
    const store = new SearchStore(undefined)

    store.currentQuery = 'asdf234'
    store.results = []

    // act
    const rendered = shallow(<ResultsList store={store} />)

    expect(rendered).to.not.be.null
    expect(rendered.find('span').text().toLowerCase()).to.contain('no results')
  })

  it('should render a single result with empty optional fields', () => {
    const store = new SearchStore(undefined)

    store.currentQuery = 'asdf234'
    store.results = [
      {
        document: {
          // author: undefined
          body: 'some test body',
          categories: ['tutorial'],
          // date: undefined
          id: 'test/1234.md',
          lang: 'en',
          relativeurl: '/test/test1',
          // tags: undefined
          title: 'test title',
        },
        id: 'test/1234.md',
        score: 0.9876,
        scoringCriteria: {},
      },
    ]

    // act
    const rendered = shallow(<ResultsList store={store} />)

    expect(rendered).to.not.be.null
    expect(rendered.text().toLowerCase()).to.not.contain('no results')

    const result = rendered.find('Result')
    expect(result.key()).to.equal('test/1234.md', 'key')

    expect(result).to.have.prop('result')
    const r = result.prop('result')
    expect(r).to.deep.equal(store.results[0])
  })

  it('should render multiple results in separate divs', () => {
    const store = new SearchStore(undefined)

    store.currentQuery = 'asdf234'
    store.results = [
      {
        document: {
          // author: undefined
          body: 'some test body',
          categories: ['tutorial'],
          // date: undefined
          id: 'test/1234.md',
          lang: 'en',
          relativeurl: '/test/test1',
          // tags: undefined
          title: 'test title',
        },
        id: 'test/1234.md',
        score: 0.9876,
        scoringCriteria: {},
      },
      {
        document: {
          author: 'gordon',
          body: 'some test body 2',
          categories: ['asdf'],
          date: '2017-04-08T15:39:47+02:00',
          id: 'test/5678.md',
          lang: 'en',
          relativeurl: '/test/test2',
          // tags: undefined
          title: 'test title 2',
        },
        id: 'test/5678.md',
        score: 0.9999,
        scoringCriteria: {},
      },
    ]

    // act
    const rendered = shallow(<ResultsList store={store} />)

    expect(rendered).to.not.be.null
    expect(rendered.text().toLowerCase()).to.not.contain('no results')

    const results = rendered.find('Result')
    expect(results).to.have.length(2, 'result.length')

    expect(results.at(0).key()).to.equal('test/5678.md', 'key')
    expect(results.at(0)).to.have.prop('result')
    let r = results.at(0).prop('result')
    expect(r).to.deep.equal(store.results[0])

    expect(results.at(1).key()).to.equal('test/1234.md', 'key')
    expect(results.at(1)).to.have.prop('result')
    r = results.at(1).prop('result')
    expect(r).to.deep.equal(store.results[1])
  })
})
