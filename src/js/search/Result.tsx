import {SearchResult, SearchStore} from 'hugo-search-index'
import * as React from 'react'

interface ISearchResultsProps {
  result: SearchResult
}

class Result extends React.Component<ISearchResultsProps, any> {

  public render() {
    const {result} = this.props
    const doc = result.document

    const date = doc.date ? new Date(doc.date).toLocaleDateString() : undefined

    const tags = (doc.tags || []).map((tag) => <span className="badge" key={tag}>{tag}</span>)

    const body = doc.body

    return (
      <div className="searchResult" key={doc.id} onClick={this.onclick.bind(this, result)}>
        <div className="header">
          <a href={doc.relativeUrl}>{doc.title || doc.name}</a>
          <span className="date">{date}</span>
        </div>
        <div className="body">
          {body}
        </div>
        <div className="tags">
          {tags}
        </div>
      </div>
    )
  }

  private onclick(result: SearchResult, event) {
    document.location.href = result.document.relativeurl
  }

}

export default Result
