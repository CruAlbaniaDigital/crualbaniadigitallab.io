/* tslint:disable:no-console */

import {SearchIndexLoader, SearchStore} from 'hugo-search-index'
import * as React from 'react'
import * as ReactDom from 'react-dom'

import ResultsList from './ResultsList'
import SearchBar from './SearchBar'

let searchIndex: any

// download the index from the server
async function downloadIndex(): Promise<Uint8Array> {
  const resp = await fetch('/search_index.gz')
  const data = await resp.arrayBuffer()

  return new Uint8Array(data)
}

// initialize the search by loading the search index and then registering the SearchWidget
export async function InitSearch(element: Element, lang?: string): Promise<void> {
  if (!searchIndex) {
    const data = await downloadIndex()

    const loader = new SearchIndexLoader()

    return new Promise<void>(
      (resolve, reject) => {
        loader.load(data, (err2, idx) => {
          if (err2) {
            console.log('Error loading search: ', err2)
            reject(err2)
            return
          }

          const store = new SearchStore(idx)

          // search index loaded, render the search bar and results
          searchIndex = idx
          ReactDom.render(
              <SearchBar store={store} lang={lang} />,
              element,
            )

          const resultsDiv = $('#' + $(element).data('resultsdiv'))

          resultsDiv.each((i, elem) => {
            const component = <ResultsList store={store} />
            ReactDom.render(component, elem)
          })

          // finish the promise
          resolve()
        })
      },
    )
  }
}

export default InitSearch
