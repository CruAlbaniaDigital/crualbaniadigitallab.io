
import { SearchResult, SearchStore } from 'hugo-search-index'
import * as React from 'react'

import Result from './Result'

interface IResultsListProps {
  store: SearchStore
}

interface IResultsListState {
  currentQuery: string,
  inProgress: boolean,
  results: SearchResult[],
  error: any,
}

class ResultsList extends React.Component<IResultsListProps, IResultsListState> {
  constructor(props) {
      super(props)
      const { store } = this.props
      this.state = {
        currentQuery: store.currentQuery,
        error: store.lastError,
        inProgress: store.inProgress,
        results: store.results,
      }
  }

  public componentDidMount() {
    const {store} = this.props
    store.on('start', (q) => {
      this.setState({
        currentQuery: q,
        error: undefined,
        inProgress: true,
      })
    })
    store.on('end', (results) => {
      this.setState({
        currentQuery: this.props.store.currentQuery,
        error: undefined,
        inProgress: false,
        results: results,
      })
    })
    store.on('error', (error) => {
      this.setState({
        error: undefined,
        inProgress: false,
        results: [],
      })
    })
  }

  public render() {
    const {currentQuery, inProgress, error, results} = this.state

    if (!currentQuery) {
      // render empty
      return null
    }

    if (inProgress) {
      return (
        <div className="searchResults">
          <div className="searching">
            <i className="fa fa-cog fa-spin"></i><span>Searching...</span>
          </div>
        </div>
      )
    }

    if (error) {
      return (
        <div className="searchResults">
          <h3 className="error">Error!</h3>
          <span className="error">{error}</span>
        </div>
      )
    }

    let resultsList
    if (results.length === 0) {
      resultsList = <span className="empty">No Results</span>
    } else {
        // sort list by score
      resultsList = results
          .sort((a, b) => b.score - a.score)
          .map((r) => <Result result={r} key={r.id} />)
    }

    return (
      <div className="searchResults">
        { resultsList }
      </div>
    )
  }
}

export default ResultsList
