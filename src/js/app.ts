/* tslint:disable:no-console */

/* tslint:disable:no-var-requires */
import 'jquery'

$('img').each(function() {
  const src = $(this).attr('src')

  if (typeof src !== typeof undefined && src as any !== false) {
    if (!window.matchMedia || !window.matchMedia('(max-width: 640px)').matches) {
        // This is not an iPhone, try to load the larger image

      const location = document.createElement('a')
      location.href = src

      if (location.hostname === '' || location.hostname === window.location.hostname) {
            // it's a locally sourced image, if it's in the /images/ directory load from /.original_images/ instead
        const path = location.pathname.split('/')
        if (path[1] === 'images') {
          path[1] = '.original_images'
          location.pathname = path.join('/')

            // preload with a detached Image element, and set it on the dom only if it succeedes.
          const $this = $(this)
          const i = new Image()
          i.onload = () => {
            // now that we've loaded and cached the image, rewrite the original image element
            $this.attr('src', location.href)
          }
          i.onerror = (ev) => {
            console.log('Error prefetching image ', location, ev)
          }
          i.src = location.href  // start loading
        }
      }
    }
  }

  return true
})

const w = window as any
const SITE_PARAMS = $('#app').data('siteparams')
w.SITE_PARAMS = SITE_PARAMS
w.LANG = $('#app').data('lang')

// load bootstrap
require('./custom_bootstrap')

// Download and load other modules on-demand.  Common code between these modules is separated out
// into a third async-loaded module.
// IMPORTANT: If you add an extra module here, add it also in the webpack.conf.js in the CommonsChunkPlugin definition

declare const require
require.ensure('./gitlab.tsx', () => {
  console.log('gitlab loaded')
  require('./gitlab')
}, 'gitlab')

$('#search-bar').each((idx, elem) => {
    $(elem).on('click.search', () => {
      $(elem).off('click.search')

      $(elem).find('.fa-search').hide()
      $(elem).find('.fa-spinner').show()

      require.ensure('./search', () => {
        const Search = require('./search').InitSearch
        console.log('search loaded: ', Search)
        const lang = window ? (window as any).LANG : undefined
        Search(elem, lang)
      }, 'search')

    })
  },
)
