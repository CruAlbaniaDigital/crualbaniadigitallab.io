/* tslint:disable:no-console */

import 'jquery'
import {autorun, observable} from 'mobx'

import {GitlabApiV4} from './gitlabv4'
import GroupStore from './store/groupStore'
import NumForksWidget from './widgets/numForksWidget'
import NumIssuesWidget from './widgets/numIssuesWidget'

const SITE_PARAMS = (window as any).SITE_PARAMS

const api = new GitlabApiV4(SITE_PARAMS.gitlab_api_token)
const store = observable(new GroupStore())

const requestList = {
    getGroup: false,
}

$('.num-forks-widget[data-source="gitlab"]').each((idx, elem) => {
    let projId = $(elem).data('projectid')
    if (!projId) {
        projId = $(elem).data('projectpath')
    }
    const button = $(elem).find('button')
    autorun((r) => {
        const proj = store.findProject(projId)
        if (!proj) {
            return
        }
        const newHtml = button.html()
            .replace(
                /forks\s?(<span class="badge">\d+<\/span>)?/i,
                `Forks <span class="badge">${proj.forks_count}</span>`)
        button.html(newHtml)
    })

    // launch a gitlab API request to populate the store
    requestList.getGroup = true
})

$('.num-issues-widget[data-source="gitlab"]').each((idx, elem) => {
    let projId = $(elem).data('projectid')
    if (!projId) {
        projId = $(elem).data('projectpath')
    }

    const button = $(elem).find('button')
    autorun((r) => {
        const proj = store.findProject(projId)
        if (!proj) {
            return
        }
        const newHtml = button.html()
            .replace(
                /issues\s?(<span class="badge">\d+<\/span>)?/i,
                `Issues <span class="badge">${proj.open_issues_count}</span>`)
        button.html(newHtml)
    })

    // launch a gitlab API request to populate the store
    requestList.getGroup = true
})

if (requestList.getGroup) {
    api.getGroup(SITE_PARAMS.gitlab).then(
        (group) => {
            // update the group - mobx takes care of the rest
            store.group = group
        },
        (err) => {
            console.log(`can't download data for group ${SITE_PARAMS.gitlab}: `, err)
        },
    )
}
