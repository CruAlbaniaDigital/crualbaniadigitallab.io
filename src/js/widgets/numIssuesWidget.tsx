import {observer} from 'mobx-react'
import * as React from 'react'

import GroupStore from '../store/groupStore'

interface INumIssuesWidgetProps {
    /** The gitlab ID of the project.  Can be the ID number or string path. */
    projectId: number | string
    store: GroupStore
}

@observer
class NumIssuesWidget extends React.Component<INumIssuesWidgetProps, any> {
    public render() {
        const proj = this.props.store.findProject(this.props.projectId)

        let issuesCount
        if (proj) {
            issuesCount = <span className="badge">{proj.open_issues_count}</span>
        }
        const href = proj ? proj.web_url + '/issues' : undefined

        return (
            <a href={href} >
            <button className="btn btn-default">
                <span className="fa fa-exclamation" /> Issues { issuesCount }
            </button>
            </a>
        )
    }
}

export default NumIssuesWidget
