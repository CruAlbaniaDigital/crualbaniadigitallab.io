import {observer} from 'mobx-react'
import * as React from 'react'

import GroupStore from '../store/groupStore'

interface INumForksWidgetProps {
    /** The gitlab ID of the project.  Can be the ID number or string path. */
    projectId: number | string
    store: GroupStore
}

@observer
class NumForksWidget extends React.Component<INumForksWidgetProps, any> {
    public render() {
        const proj = this.props.store.findProject(this.props.projectId)

        let forksCount
        if (proj) {
            forksCount = <span className="badge">{ proj.forks_count }</span>
        }
        const href = proj ? proj.web_url + '/forks' : undefined

        return (
            <a href={href} >
            <button className="btn btn-default">
                <span className="fa fa-code-fork" /> Forks {forksCount}
            </button>
            </a>
        )
    }
}

export default NumForksWidget
