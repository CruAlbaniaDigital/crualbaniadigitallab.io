/* tslint:disable:no-unused-expression */
import * as chai from 'chai'
import * as chaiEnzyme from 'chai-enzyme'
import { shallow } from 'enzyme'
import * as React from 'react'
import * as sinon from 'sinon'

chai.use(chaiEnzyme()) // Note the invocation at the end
const expect = chai.expect

import {Project} from '../gitlabv4/responses'
import GroupStore from '../store/groupStore'
import NumForksWidget from './numForksWidget'

describe('<NumForksWidget />', () => {
    it('should display a badge for the forks_count property of the project', () => {
        const store = new GroupStore()
        const stub = sinon.stub(store, 'findProject')
        stub.withArgs(1234567).returns(new Project({
            forks_count: 12,
        }))

        // act
        const rendered = shallow(<NumForksWidget store={store} projectId={1234567} />)

        expect(rendered).to.contain(<span className="badge">{12}</span>)
    })

    it('should handle project path in addition to project id', () => {
        const store = new GroupStore()
        const stub = sinon.stub(store, 'findProject')
        stub.withArgs('test/1234').returns(new Project({
            forks_count: 74,
        }))

        // act
        const rendered = shallow(<NumForksWidget store={store} projectId={'test/1234'} />)

        expect(rendered).to.contain(<span className="badge">{74}</span>)
    })

    it('should handle undefined project without throwing', () => {
        const store = new GroupStore()
        const stub = sinon.stub(store, 'findProject')
        stub.withArgs(1234567).returns(undefined)

        // act
        const rendered = shallow(<NumForksWidget store={store} projectId={1234567} />)

        expect(rendered.find('.badge')).to.be.empty
    })
})
