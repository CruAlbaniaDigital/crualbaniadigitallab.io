import {computed, observable} from 'mobx'

import {Group, Project} from '../gitlabv4/responses'

class GroupStore {
    @observable public group: Group

    @computed get projects(){
        return this.group.projects
    }

    constructor() {
        this.group = new Group()
    }

    public getProjectById(id: number): Project {
        if (!this.group) {
            return undefined
        }

        return this.group.projects.find((p) => p.id === id)
    }

    public getProjectByPath(path: string): Project {
        if (!this.group) {
            return undefined
        }

        return this.group.projects.find((p) => p.path_with_namespace === path)
    }

    public findProject(idOrPath: number | string): Project {
        if (!this.group) {
            return undefined
        }
        if (typeof idOrPath === 'number') {
            return this.group.projects.find((p) => p.id === idOrPath)
        }
        if (typeof idOrPath === 'string') {
            return this.group.projects.find((p) => p.path_with_namespace.toLowerCase() === idOrPath.toLowerCase())
        }
        throw new Error('Unable to find projects using ID of type ' + typeof idOrPath)
    }
}

export default GroupStore
