import { Commit, Contributor, Group, Issue, Project } from './responses'

const API_V4 = 'https://gitlab.com/api/v4/'

export class GitlabApiV4 {
    private API_TOKEN: string
    private apiUrl: string

    constructor(apiToken: string, apiUrl?: string) {
        this.API_TOKEN = apiToken
        this.apiUrl = apiUrl || API_V4
    }

    public async getProject(id: number): Promise<Project> {
        const resp = await fetch(this.apiUrl + `projects/${id}`, {
            headers: {
                'PRIVATE-TOKEN': this.API_TOKEN,
            },
        })

        if (resp.status !== 200) {
            throw new Error(`error getting project ${id}: ${resp.status} ` + resp.statusText)
        }

        const body = await resp.json()
        return new Project(body)
    }

    public async getGroup(name: string): Promise<Group> {
        const resp = await fetch(this.apiUrl + `groups/${name}/`, {
            headers: {
                'PRIVATE-TOKEN': this.API_TOKEN,
            },
        })

        if (resp.status !== 200) {
            throw new Error(`error getting group ${name}: ${resp.status} ` + resp.statusText)
        }

        const body = await resp.json()
        return new Group(body)
    }

    public async getCommits(projectId: number): Promise<Commit[]> {
        const resp = await fetch(this.apiUrl + `projects/${projectId}/repository/commits`, {
            headers: {
                'PRIVATE-TOKEN': this.API_TOKEN,
            },
        })

        if (resp.status !== 200) {
            throw new Error(`error getting commits for ${projectId}: ${resp.status} ` + resp.statusText)
        }

        const body = await resp.json()
        return body.map((obj) => new Commit(obj))
    }

    public async getContributors(projectId: number): Promise<Contributor[]> {
        const resp = await fetch(this.apiUrl + `projects/${projectId}/repository/contributors`, {
            headers: {
                'PRIVATE-TOKEN': this.API_TOKEN,
            },
        })

        if (resp.status !== 200) {
            throw new Error(`error getting contributors for ${projectId}: ${resp.status} ` + resp.statusText)
        }

        const body = await resp.json()
        return body.map((obj) => new Contributor(obj))
    }

    public async getOpenIssues(projectId: number): Promise<Issue[]> {
        const resp = await fetch(this.apiUrl + `projects/${projectId}/issues?state=opened`, {
            headers: {
                'PRIVATE-TOKEN': this.API_TOKEN,
            },
        })

        if (resp.status !== 200) {
            throw new Error(`error getting issues for ${projectId}: ${resp.status} ` + resp.statusText)
        }

        const body = await resp.json()
        return body.map((obj) => new Issue(obj))
    }
}
