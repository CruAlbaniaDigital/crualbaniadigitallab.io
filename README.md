# CRU Albania Digital Strategies web page

[![Join the chat at https://gitter.im/campuscrusade/lobby](https://badges.gitter.im/campuscrusade/lobby.svg)](https://gitter.im/campuscrusade/lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

This is the repository for the webpage [crualbaniadigital.gitlab.io](http://crualbaniadigital.gitlab.io/) which is the display site for all of CRU Albania's open source projects.

The website is based off of [Victor Hugo from Netlify](https://github.com/netlify/victor-hugo/), using gulp and hugo to build a static site and deploy it to Gitlab Pages.  The theme is the [Hugo Code Editor Theme](https://themes.gohugo.io/hugo-code-editor-theme/) with custom changes and widgets.

All the content is Copyright CRU, 2017.  The Software is based on open-source, and if desired we're happy to split off a template project with a permissive license for you to use.  [Contact Us](http://crualbaniadigital.gitlab.io/contact_us/).